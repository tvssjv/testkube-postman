import httpx
import asyncio
import time
import os
import sys
from pprint import pprint as print
import re
import requests
import json

COLLECTIONS_FOLDER_PATH = "./postman-collections"
URLs = {
    "test": "http://192.168.106.2:8765/v1/tests",
    "test_suite": "http://192.168.106.2:8765/v1/test-suites",
    "start_test_suite": "http://192.168.106.2:8765/v1/test-suites/{test_suite_name}/executions",
    "get_status": "http://192.168.106.2:8765/v1/test-suites/{test_suite_name}/executions/{id}"
}

test_suite_steps = []


def get_test_suite_step(name, namespace):
    return {
        "stopTestOnFailure": False,
        "execute": {
            "namespace": namespace,
            "name": name
        }
    }


def prepare_body(file_name):
    file_content = open(COLLECTIONS_FOLDER_PATH + "/" + file_name, "r").read()
    name = '-'.join([file_name.split('.')[0], job_id])
    namespace = "testkube"
    test_suite_steps.append(get_test_suite_step(name, namespace))
    body = {
        "name": name,
        "namespace": namespace,
        "type": "postman/collection",
        "labels": {
            "job_id": job_id
        },
        "content": {
            "type": "string",
            "data": file_content
        }
    }
    return body


async def get_async(file):
    body = prepare_body(file)
    f = open(COLLECTIONS_FOLDER_PATH + "/" + file, "rb")
    async with httpx.AsyncClient() as client:
        return await client.post(url=URLs['test'], json=body)


async def launch():

    resps = await asyncio.gather(*map(get_async, files))
    data = [(resp.status_code, resp.json()) for resp in resps]

    for status_code in data:
        print(status_code)


def check_valid_name(file_name):
    valid_pattern = '^([a-zA-Z0-9])([a-zA-Z0-9.-])*([a-zA-Z0-9])$'
    return re.match(valid_pattern, file_name) is not None


def create_test_suite():
    name = '-'.join(["test-suite", job_id])
    namespace = "testkube"

    body = {
        "name": name,
        "namespace": namespace,
        "steps": test_suite_steps
    }

    response = requests.post(URLs["test_suite"], json=body)
    print(response.content)
    return name


def delete_tests():
    response = requests.delete(url=URLs["test"])
    print(response.content)


def delete_test_suites():
    response = requests.delete(url=URLs["test_suite"])
    print(response.content)


def start_test_suite(test_suite_name):
    body = {
        "name": test_suite_name,
        "namespace": "testkube",
        "variables": {
        },
        "labels": {
            "job_id": job_id
        },
        "executionLabels": {
            "job_id": job_id
        },
        "sync": True
    }

    url = URLs["start_test_suite"].format(test_suite_name=test_suite_name)
    response = requests.post(url, json=body)
    with open("execution_response.json", "w") as f:
        json.dump(response.json(), f)


def get_status(test_suite_name, execution_id):
    url = URLs["get_status"].format(
        test_suite_name=test_suite_name, id=execution_id)
    response = requests.get(url=url)
    json_response = response.json()
    return json_response["status"]


def start_test_suite_async(test_suite_name):
    body = {
        "name": test_suite_name,
        "namespace": "testkube",
        "variables": {
        },
        "labels": {
            "job_id": job_id
        },
        "executionLabels": {
            "job_id": job_id
        },
        "sync": False
    }

    url = URLs["start_test_suite"].format(test_suite_name=test_suite_name)
    response = requests.post(url, json=body)
    with open("execution_response.json", "w") as f:
        json.dump(response.json(), f)

    json_response = response.json()
    execution_id = json_response["id"]

    while True:
        time.sleep(1)
        status = get_status(test_suite_name, execution_id)
        print("status: {}".format(status))
        if status in {"passed", "failed"}:
            break



if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit("job id should be given as argument")

    delete_tests()
    delete_test_suites()

    job_id = sys.argv[1]
    files = os.listdir(COLLECTIONS_FOLDER_PATH)

    # creates individual tests and creates part of payload for creating test-suite
    asyncio.run(launch())

    # creates testsuite
    test_suite_name = create_test_suite()

    print(test_suite_name)
    # start_test_suite(test_suite_name)
    start_test_suite_async(test_suite_name)
